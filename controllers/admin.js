const Product = require('../models/product');


exports.getAddProduct = (req, res, next) => {
    res.render('admin/add-product', { 
        pageTitle: 'Add Product',
        path: '/admin/add-product'
    });
}

exports.postAddProduct = (req, res, next) => {
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const price = req.body.price;
    const description = req.body.description;
    const product = new Product({
        title: title, 
        price:price,
        description: description,
        imageUrl: imageUrl,
        userId: req.user
    })

    /* // Mongodb raw
    const product = new Product(title, price, description, imageUrl, null, req.user._id)
    */
   
    product.save()
    .then(result => {
        console.log('Product Created')
        res.redirect('/admin/products');
    }).catch(err => {
        console.log(err)
    })

    /* // sequlize
    req.user.createProduct({
    //Product.create({
        title:title,
        price:price,
        imageUrl:imageUrl,
        description:description,
        // userId:req.user.id 또는 위의 방법
    }).then(result => {
        console.log('Product Created')
        res.redirect('/admin/products');
    }).catch(er => {
        console.log(err)
    })
    */

    /* Raw mysql
    const product = new Product(null, title, imageUrl, description, price);
    product.save().then(() => {
        res.redirect('/')    
    })
    .catch(err=> console.log(err));*/
}

exports.getEditProduct = (req, res, next) => { 
    const prodId = req.params.productId;

    Product.findById(prodId)
    .then( product => {
        if(!product) {
            return res.redirect('/');
        }
        res.render('admin/edit-product', { 
            pageTitle: 'Edit Product',
            path: '/admin/edit-product',
            //editing: editMode,
            product:product
        });
    })
    .catch(err => {
        console.log(err)
    });

    /* // sequlize
    req.user.getProducts({where: {id:prodId}})
    //Product.findById(prodId)
    .then( products => {// product => {
        const product =products[0]
        if(!product) {
            return res.redirect('/');
        }
        console.log(product);
        res.render('admin/edit-product', { 
            pageTitle: 'Edit Product',
            path: '/admin/edit-product',
            //editing: editMode,
            product:product
        });
    })
    .catch(err => {
        console.log(err)
    });
    */
}

exports.postEditProduct = (req, res, next) => {
    const prodId = req.params.productId;
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const price = req.body.price;
    const description = req.body.description;

    Product.findById(prodId).then(product => {
        product.title = title
        product.imageUrl = imageUrl
        product.price = price
        product.description = description
        return product.save()
    })
    .then(result => {
        console.log('Updated Product')
        res.redirect('/admin/products');
    }).catch(err => {
        console.log(err)
    })

    /* // mongo raw
    const product = new Product(
        title, 
        price, 
        description, 
        imageUrl, 
        prodId )

    product.save()
    .then(result => { 
        console.log('Updated Product')
        res.redirect('/admin/products');
    })
    .catch(err => {
        console.log(err)
    })*/

    /* // sequalize
    Product.findById(prodId)
    .then(product => {
        product.title = title
        product.imageUrl = imageUrl
        product.price = price
        product.description = description
        return product.save()
    })
    .then(result => { 
        console.log('Updated Product')
        res.redirect('/admin/products');
    })
    .catch(err => {
        console.log(err)
    })
    */
}

exports.postDeleteProduct = (req, res, next) => {
    const prodId = req.params.productId;
    Product.findByIdAndRemove(prodId)
    // mongo raw Product.deleteById(prodId)
    .then(() => {
        console.log('Product destroyed')
        res.redirect('/admin/products');
    })
    .catch(err => {
        console.log(err)
    })

    /* // sequalize
    const prodId = req.params.productId;
    Product.findById(prodId)
    .then(product => {
        return product.destroy()
    })
    .then(result => {
        console.log('Product destroyed')
        res.redirect('/admin/products');
    })
    .catch(err => {
        console.log(err)
    })*/
    
}

exports.getProducts = (req, res, next) => {
    Product
    .find()
    //.populate('userId')
    // mongo raw .fetchAll()
    .then( products => {
        res.render('admin/products', {
            prods: products, 
            pageTitle: 'Admin / Products',
            path: '/admin/products'
        });
    })
    .catch( err => {
        console.log(err)
    })


    /* // sequlize
    //Product.findAll()
    req.user.getProducts()
    .then( products => {
        res.render('admin/products', {
            prods: products, 
            pageTitle: 'Admin / Products',
            path: '/admin/products'
        });
    })
    .catch( err => {
        console.log(err)
    })

     // raw mysql
    Product.fetchAll( products => {
        res.render('admin/products', {
            prods: products, 
            pageTitle: 'Admin / Products',
            path: '/admin/products'
        });
    }); */
}