const Product = require('../models/product');
//const Cart = require('../models/cart');
const Order = require('../models/order');


exports.getProducts = (req, res, next) => {
    Product
        .find()
        // mongoraw .fetchAll()
        .then( products => {
        res.render('shop/product-list', {
            prods: products, 
            pageTitle: 'Products',
            path: '/Products'
        });
    }).catch( err => {
        console.log(err)
    })
    /* // sequalize
    Product.findAll().then( products => {
        res.render('shop/product-list', {
            prods: products, 
            pageTitle: 'Products',
            path: '/Products'
        });
    }).catch( err => {
        console.log(err)
    })
    */

    /* raw mysql
    Product.fetchAll()
    .then(([rows, fieldData]) => {
        res.render('shop/product-list', {
            prods: rows, 
            pageTitle: 'Products',
            path: '/Products'
        });
    })
    .catch(err => {
        console.log(err)
    }) */
}

exports.getProduct = (req, res, next) => {
    const prodId = req.params.productId;

    Product.findById(prodId)
    .then( product =>{ // ([product, catalog]) => { //raw mysql
        res.render('shop/product-detail', {
            product: product,// product[0], //raw mysql
            pageTitle: product.title,
            path: '/products'
        });
    })
    .catch((err) => {
        console.log(err)
    });

    /* // sequlize // findAll 이용 시
    Product.findAll({where:{id:prodId}})
    .then(products => {
        res.render('shop/product-detail', {
            product:  product[0], 
            pageTitle: product[0].title,
            path: '/products'
        });
    })
    .catch()

     // findById 함수 이용
    Product.findById(prodId)
    .then( product =>{ // ([product, catalog]) => { //raw mysql
        res.render('shop/product-detail', {
            product: product,// product[0], //raw mysql
            pageTitle: product.title,
            path: '/products'
        });
    })
    .catch((err) => {
        console.log(err)
    });*/
}

exports.getIndex = (req, res, next) => {
    Product
        .find()
        // mongoraw .fetchAll()
        .then( products => {
        res.render('shop/index', {
            prods: products, 
            pageTitle: 'Home',
            path: '/'
        });
    }).catch( err => {
        console.log(err)
    })
    /* // sequalize
    Product.findAll().then( products => {
        res.render('shop/index', {
            prods: products, 
            pageTitle: 'Home',
            path: '/'
        });
    }).catch( err => {
        console.log(err)
    })
    */

    /* raw mysql
    Product.fetchAll()
    .then(([rows, fieldData]) => {
        res.render('shop/index', {
            prods: rows, 
            pageTitle: 'Home',
            path: '/'
        });
    })
    .catch(err => {
        console.log(err)
    }) */
}

exports.getCart = (req, res, next) => {
    req.user
        .populate('cart.items.productId')
        .execPopulate()
        //.getCart()
        //.then( products => {
        .then( user => {   
            const products = user.cart.items
            res.render('shop/cart', {
                path:'/cart',
                pageTitle:'Cart',
                products: products
            });
        })
        .catch( err => console.log(err))
    
    /* // squelize
    req.user
        .getCart()
        .then( cart => {
            return cart.getProducts()
                .then( products => {
                    res.render('shop/cart', {
                        path:'/cart',
                        pageTitle:'Cart',
                        products: products
                    });
                })
                .catch(err => console.log(err))
        })
        .catch( err => console.log(err))

      // Raw Mysql
    Cart.getCart(cart => {
        console.log(cart)
        Product.fetchAll(products => {
            const cartProducts = [];
            console.log(products)
            for(product of products){
                console.log(product)
                const cartProductData = cart.products.find(prod => prod.id === product.id);
                if(cartProductData){
                    console.log(product);
                    cartProducts.push({productData:product, qty: cartProductData.qty});
                }
            }
            console.log(cartProducts);
            res.render('shop/cart', {
                path:'/cart',
                pageTitle:'Cart',
                products: cartProducts
            });
        });
        
    });*/ 
}

exports.postCart = (req, res, next) => {
    const prodId = req.params.productId;
    Product.findById(prodId).then(product => {
        return req.user.addToCart(product)
    }).then( result => {
        res.redirect('/cart')
    })
    
    /* // sequalize
    let fetchedCart
    let newQuantity = 1

    req.user.getCart()
        .then(cart => {
            fetchedCart = cart
            return cart.getProducts({where: {id:prodId}})
        })
        .then(products => {
            let product
            if(products.length > 0){
                product = products[0]
            } 
            
            if(product){
                const oldQuantity = product.cartItem.quantity // 중간 연결 테이블 데이터를 가지고 다님.
                newQuantity = oldQuantity + 1
                return product
            }
            return Product.findByPk(prodId)
        })
        .then(product => {
            return fetchedCart.addProduct(product, {
                through:{quantity: newQuantity} 
            }) // sequelize magic 함수
        })
        .then( result => {
            res.redirect('/cart')
        })
        .catch(err => console.log(err))
        */

    /* // raw mysql
    Product.findById(prodId, (product) => {
        Cart.addProduct(prodId, product.price);
    });
    res.redirect('/cart');
    */
};

exports.postCartDeleteProduct = (req, res, next) => {
    const prodId = req.params.productId;

    req.user
        .removeFromCart(prodId)
        .then(result => {
            res.redirect('/cart');
        })
        .catch(err => console.log(err))
}

/*exports.getCheckout = (req, res, next) => {
    res.render('shop/checkout', {
        path:'/checkout',
        pageTitle:'Checkout'
    });
}*/

exports.getOrders = (req, res, next) => {
    Order.find({"user.userId":req.user._id})
        .then(orders => {
            res.render('shop/orders', {
                path:'/orders',
                pageTitle:'Orders',
                orders:orders
            });
        })
        .catch(err => console.log(err))
}


exports.postOrder = (req, res, next) => {
    req.user
        .populate('cart.items.productId')
        .execPopulate()
        .then( user => {   
            const products = user.cart.items.map(i => {
                return {quantity: i.quantity, product: { ...i.productId._doc} };
            });
            const order = new Order({
                user:{
                    name:req.user.name,
                    userId:req.user
                },
                products:products
            });
            return order.save();
        })
        .then(result => {
            return req.user.clearCart();
        })
        .then(() => {
            res.redirect('/orders');
        })
        .catch(err => console.log(err))
}