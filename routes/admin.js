//const path = require('path');
//const rootDir = require('../util/path');
const express = require('express');

const adminController = require('../controllers/admin');

const router = express.Router();



router.get('/add-product', adminController.getAddProduct);

router.get('/products', adminController.getProducts);


router.post('/add-product', adminController.postAddProduct);


router.get('/edit-product/:productId', adminController.getEditProduct);
router.post('/edit-product/:productId', adminController.postEditProduct);

router.get('/delete-product/:productId', adminController.postDeleteProduct);

//exports.routes = router;
module.exports = router;
