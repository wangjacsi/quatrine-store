//const path = require('path');
//const rootDir = require('../util/path');
const express = require('express');

const router = express.Router();

const shopController = require('../controllers/shop');

router.get('/', shopController.getIndex);

router.get('/products', shopController.getProducts);

// product/3231
router.get('/product/:productId', shopController.getProduct);


router.get('/cart', shopController.getCart);
router.get('/cart/:productId', shopController.postCart); // post
router.post('/cart-delete-item/:productId', shopController.postCartDeleteProduct);

//router.get('/checkout', shopController.getCheckout);
router.get('/orders', shopController.getOrders);
router.post('/create-order', shopController.postOrder);

module.exports = router;