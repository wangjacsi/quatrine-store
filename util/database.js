const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient

let _db;

const mongoConnect = (callback) => {
    MongoClient.connect('mongodb+srv://sean:jX7eiROtGA8uL2ox@cluster0-jpznq.mongodb.net/shop?retryWrites=true')
    .then(client => {
        console.log('Connected!')
        _db = client.db()
        callback()
    })
    .catch(err => {
        console.log(err)
        throw err
    })
}

const getDb = () => {
    if(_db) {
        return _db
    }
    throw 'No Database found!'
}

//module.exports = mongoConnect
exports.mongoConnect = mongoConnect
exports.getDb = getDb
