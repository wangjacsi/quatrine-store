const path = require('path');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// Database
// const db = require('./util/database'); // Raw Mysql

/* // Sequalize
const sequelize = require('./util/database')
const User = require('./models/user')
const Product = require('./models/product')
const Cart = require('./models/cart')
const CartItem = require('./models/cart-item')
const Order = require('./models/order')
const OrderItem = require('./models/order-item')
*/

// mongodb
// const mongoConnect = require('./util/database').mongoConnect
// Change to Mongoose 
const mongoose = require('mongoose')

// user model
const User = require('./models/user')

app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const errorController = require('./controllers/error');


app.use(bodyParser.urlencoded({extended: false}));
// app.use(express.urlencoded({extende:false})) 같음
app.use(express.static(path.join(__dirname, 'public')));


app.use((req, res, next) => {
    User.findById('5c23963aa6e9458a5f2c1007')
    .then(user => {
        //console.log(user)
        req.user = user//new User(user.name, user.email, user.cart, user._id)
        next()
    }).catch(err => {
        console.log(err)
    })
})

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

// mongoose
mongoose
    .connect('mongodb+srv://sean:1290qwopaskl@cluster0-jpznq.mongodb.net/shop?retryWrites=true')
    .then( result => {
        User.findOne().then(user => {
            if(!user){
                const user = new User({
                    name: 'Sean',
                    email:'wangjacsi@gmail.com',
                    cart:{
                        items:[]
                    }
                })
                user.save()
            }
        })
        app.listen(3000)
    })
    .catch(err => {
        console.log(err)
    })

/* // Sequalize
// 관계 정의
// User-Product => One-to-Many
Product.belongsTo(User, {constraints:true, onDelete:'CASCADE'})
User.hasMany(Product)
// User-Cart => One-to-One
User.hasOne(Cart)
Cart.belongsTo(User)
// Cart-Product => Many-to-Many
Cart.belongsToMany(Product, {through : CartItem })
Product.belongsToMany(Cart, {through : CartItem })
// User - Order => One-to-Many
Order.belongsTo(User)
User.hasMany(Order)
// Order-Product => Many-to-Many
Order.belongsToMany(Product, {through:OrderItem})



sequelize
    .sync() // 
    //.sync({force:true})
    .then(result => {
        return User.findById(1)
        //app.listen(3000);
    })
    .then( user => {
        if(!user) {
            return User.create({name:'sean', 'email':'test@test.com'})
        } 
        return user
    })
    //.then( user => {
    //    return user.createCart()
    //})
    .then( user => {
        app.listen(3000);
    })
    .catch(err => {
        console.log(err)
    })
*/

/* // mongo connect
mongoConnect(() => {
    app.listen(3000)
})
*/
